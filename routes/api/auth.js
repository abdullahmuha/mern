const express = require("express");
const router = express.Router();
const auth = require("../../middleware/auth");
const User = require("../../models/User");
const { check, validationResult } = require("express-validator/check");
const bccrypt = require("bcryptjs");
const jwr = require("jsonwebtoken");
const config = require("config");

router.get("/", auth, async (req, res) => {
  try {
    const user = await User.findById(req.user.id).select("-password");
    res.json({ user });
  } catch (err) {
    console.error(err.message);
    res.status(500).send("server error");
  }
});

router.post(
  "/",
  [
    check("email", "email not correct").isEmail(),
    check("password", "enter pass").exists(),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body;
    try {
      let user = await User.findOne({ email });
      if (!user) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Credenziali non valide" }] });
      }

      const ismatch = await bccrypt.compare(password, user.password);
      if (!ismatch) {
        return res
          .status(400)
          .json({ errors: [{ msg: "Credenziali non valide" }] });
      }
      const payaload = {
        user: {
          id: user.id,
        },
      };
      jwr.sign(
        payaload,
        config.get("jwt"),
        { expiresIn: 7200 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.log(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
