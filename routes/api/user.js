const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator/check");
const jwr = require("jsonwebtoken");
const config = require("config");
const User = require("../../models/User");
const gravatar = require("gravatar");
const bccrypt = require("bcryptjs");
router.post(
  "/",
  [
    check("name", "name is required").not().isEmpty(),
    check("email", "email not correct").isEmail(),
    check("password", "enter pass").isLength({ min: 6 }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body;
    try {
      let user = await User.findOne({ email });
      if (user) {
        return res.status(400).json({ errors: [{ msg: "gia esistente" }] });
      }
      const avatar = gravatar.url(email, { s: "200", r: "pg", d: "mm" });
      user = new User({ name, email, avatar, password });
      const salt = await bccrypt.genSalt(10);
      user.password = await bccrypt.hash(password, salt);
      await user.save();

      const payaload = {
        user: {
          id: user.id,
        },
      };
      jwr.sign(
        payaload,
        config.get("jwt"),
        { expiresIn: 360000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.log(err.message);
      res.status(500).send("Server error");
    }
  }
);

module.exports = router;
