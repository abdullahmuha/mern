import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import Spinner from "../layout/Spinner";
import { getProfile } from "../../actions/profile";
import { connect } from "react-redux";
import ProfileItem from "./ProfileItem";

const Profiles = ({ getProfile, profile: { profiles, loading } }) => {
  useEffect(() => {
    getProfile();
  }, [getProfile]);
  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <h1 className="large text-primary">Devlopers</h1>
          <p className="lead">
            <i className="fab fa-connectdevelop"></i> Browse and connect with
            devlopers
          </p>
          <div className="profiles">
            {profiles.length > 0 ? (
              profiles.map((profile) => (
                <ProfileItem key={profile._id} profile={profile}></ProfileItem>
              ))
            ) : (
              <h4>No Profiles found...</h4>
            )}
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

Profiles.propTypes = {
  getProfile: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  profile: state.profile,
});
export default connect(mapStateToProps, { getProfile })(Profiles);
